﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mime;
using System.Runtime.CompilerServices;
using Livingstone.DBLib;

namespace CoupaInvoicenew
{
    class Program
    {
        private static IDBHandler dbHandler = new SqlServerDB();
        static void Main(string[] args)
        {
            String Invoice = "7435274";

            string invoicelink = GetInvoiceLink(Invoice);


            String pdf;

            try
            {
                WebClient wc = new WebClient();

                 pdf = wc.DownloadString(invoicelink)  ;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            String pdfBase64string = EncodeTo64(pdf);

            Console.WriteLine(pdfBase64string);


        }


        public static String EncodeTo64(string file)
        {
            byte[] toBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(file);

            return System.Convert.ToBase64String(toBytes);

        }



        public static String GetInvoiceLink(String Invoice)
        {


            String GeneratePDFsql = @"exec dbo.GetcXMLInvoicePDF @invoiceNo";

            GeneratePDFsql = GeneratePDFsql.Replace("@invoiceNo", Invoice);

            dbHandler.ExecuteNonQuery(GeneratePDFsql, SqlServerDB.Servers.netcrmau);


            var address = new List<List<string>>();

            String GetaddressSql = @"SELECT InvoicePDF FROM cXMLInvoiceLog where invNo = '@invoiceNo' ";
            GetaddressSql = GetaddressSql.Replace("@invoiceNo", Invoice);

            dbHandler.getDataList(null, address, null, GetaddressSql, SqlServerDB.Servers.netcrmau);

            String invoicelink = address[0][0].ToString();

            return invoicelink;

        }



    }
}
